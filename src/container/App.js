import React, { Component } from 'react';
import Field from '../components/Field/Field'
import Title from '../components/Title/Title'
import Footer from '../components/Footer/Footer'
import './App.css';

class App extends Component {
	
    constructor(props) {
		super(props);
		this.state = {
			game: false,
			score: 0
		}
	}

	gameStart = () => {
		this.setState({
			game: true,
			score: 0
		})
	}

	gameOver = () => {
		this.setState({
			game: 'game_over'
		})
	}

	score = () => {
		let score = this.state.score;
		this.setState({
			score: ++score
		})
	}

	render() {

		switch (this.state.game) {
			case true:
			return (
				<section className="game">
					<Field gameOver={this.gameOver} score={this.score} />
					<Footer />
				</section>
			)
			case false:
			return (
				<section className="game">
					<Title game={this.state.game} gameStart={this.gameStart} />
					<Footer />
				</section>
			);
			case 'game_over':
			return (
				<section className="game">
					<Title score={this.state.score} game={this.state.game} gameStart={this.gameStart} />
					<Footer />
				</section>
			)
			default:
			return (
				<section className="game">
					<Title game={false} gameStart={this.gameStart} />
					<Footer />
				</section>
			);
		}
		
	}
}

export default App;