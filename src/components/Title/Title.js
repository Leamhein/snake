import React, { Component } from 'react';
import './Title.css';

class Title extends Component {

    render () {
        switch (this.props.game) {
            case false:
                return (
                    <div className="title_screen">
                        <p>lets play snake game</p>
                        <button onClick={this.props.gameStart} >play</button>
                    </div>
                )

            case 'game_over':
                return (
                    <div className="title_screen">
                        <p>your score</p>
                        <p>{this.props.score}</p>
                        <button onClick={this.props.gameStart} >play</button>
                    </div>
                ) 
        
            default:
                break;
        }
        
    }
}

export default Title