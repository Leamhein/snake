import React from 'react'
import './Footer.css'

class Footer extends React.Component {
    render () {
        return (
			<footer>
				<div className="info">
					<p>Game by Vladimir Apenko</p>
					<p>for TouchSoft</p>
				</div>
				<div className="social">
					<a href="mailto:leamhein@hotmail.com?subject=Match-Match Game"></a>
					<a href="https://www.linkedin.com/in/leamhain/?locale=en_US"></a>
					<a href="https://www.facebook.com/Leamhain"></a>
				</div>
			</footer>
        )
    }
};

export default Footer