import React, {Component} from 'react'
import './ControlPanel.css'

class ControlPanel extends Component {
    render () {
        return (
            <div className="control">
                <button onClick={(e)=>{this.props.control(e)}}
                 className={'up'+(this.props.direction === 'up'?' select' : '')}>&#8593;</button>
                <button onClick={(e)=>{this.props.control(e)}}
                 className={'left'+(this.props.direction=='left' ? ' select' : '')}>&#8592;</button>
                <button onClick={(e)=>{this.props.control(e)}}
                 className={'down'+(this.props.direction=='down' ? ' select' : '')}>&#8595;</button>
                <button onClick={(e)=>{this.props.control(e)}}
                 className={'right'+(this.props.direction=='right' ? ' select' : '')}>&#8594;</button>
            </div>
        )
    }
}

export default ControlPanel