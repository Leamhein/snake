import React, { Component } from 'react';
import Blocks from '../Blocks/Blocks'
import ControlPanel from '../ControlPanel/ControlPanel'
import './Field.css';

class Field extends Component {
	
	constructor(props) {
		super(props);
		this.state = {
			move: 'up',
			isSnake: false,
			snakePosition: [],
			isApple: false,
			applePosition: []
		}
	}
	//start game loop and key event listener
	componentDidMount = () => {
		this.gameLoop = setInterval(this.tick, 300);
		document.addEventListener('keydown', this.onKeyPressed);
	};
	//when this component will unmount -> stop game loop and listener
	componentWillUnmount = () => {
		clearInterval(this.gameLoop);
		document.removeEventListener('keydown', this.onKeyPressed);
	}
	//check the key press
	onKeyPressed = (e) => {
		switch (e.key) {
			case 'ArrowUp':
				if (this.state.move === 'down') {
					break;
				}
				this.setState({
					move: 'up'
				})
				break;
				
			case 'ArrowDown':
				if (this.state.move === 'up') {
					break;
				}
				this.setState({
					move: 'down'
				})
				break;
			
			case 'ArrowLeft':
				if (this.state.move === 'right') {
					break;
				}
				this.setState({
					move: 'left'
				})
				break;

			case 'ArrowRight':
				if (this.state.move === 'left') {
					break;
				}
				this.setState({
					move: 'right'
				})
				break;

			default:
				break;
		}
	}
	// this is the main function of our game loop. Here we:
	tick = () => {
		//set random position of the snake
		if (!this.state.isSnake) {
			this.createSnake();
		}
		//set random position of the apple
		if (!this.state.isApple) {
			this.createApple();
		}
		this.move();

	}

	createSnake = () => {
		/*I generate 1<i<19 and 1<j<20. I want to rule out the situation
		when the snake is generated just before the wall,
		because in this situation next step is game over*/
		let i = Math.round(Math.random() * 16)+2;
		let j = Math.round(Math.random() * 17)+2;
		this.setState({
			snakePosition: [[i,j], [i+1,j], [i+2,j]],
			isSnake: true
		})
	}

	createApple = () => {
		/*I generate 0<=i<20 and 0<=j<20.
		There is no such problem with apple*/
		let i = Math.round(Math.random() * 19);
		let j = Math.round(Math.random() * 19);
		const position = [i , j]
		this.setState({
			isApple: true,
			applePosition: position
		})
	}
	//here I change the coordinates of the snake's head depending on the direction
	move = () => {
		let snakeHead = this.state.snakePosition[0];
		let x = snakeHead[1];
		let y = snakeHead[0];
		switch (this.state.move) {
			case 'up':
				y--
				break;
			case 'down':
				y++
				break;
			case 'left':
				x--
				break;
			case 'right':
				x++
				break;
			default:
        		y--;

		}
		//check situation when this game is over
		this.gameOverCheck(y, x);
		//check apple eat
		this.appleEatCheck(y, x);

	}

	gameOverCheck = (y, x) => {
		//if the snake hits the wall -> game over
		if (x < 0 || x > 19 || y < 0 || y > 19) {
			this.props.gameOver();
		}
		//if the snake eat itself -> game over
		if (this.state.snakePosition.some((item)=>{
			return item[0]===y && item[1]===x
		})) {
			this.props.gameOver();
		}
	}

	appleEatCheck = (y, x) => {
		let snakePos = [[y, x]];
		//check position of the snake's head and apple
		if (this.state.applePosition[0] === y && this.state.applePosition[1] === x) {
			//if true -> generate new apple
			this.setState({
				isApple: false,
				applePosition: []
			})
			//score+=1
			this.props.score();
			//snakes length+=1
			snakePos = snakePos.concat(this.state.snakePosition);
		} else {
			snakePos = snakePos.concat(this.state.snakePosition);
			snakePos.length = this.state.snakePosition.length;
		}

		this.setState({
			snakePosition: snakePos
		})
	}

	control = (e) => {
		const direction = e.target.className;
		if (direction === 'up' && this.state.move === 'down') {
			return;
		}
		if (direction === 'down' && this.state.move === 'up') {
			return;
		}
		if (direction === 'left' && this.state.move === 'right') {
			return;
		}
		if (direction === 'right' && this.state.move === 'left') {
			return;
		} 
		this.setState({
			move: direction
		})
	}

	render () {
		return (
			<div className='field'>
				<Blocks snakePos={this.state.snakePosition} 
				applePos={this.state.applePosition} />
				<ControlPanel control={this.control}
				 direction={this.state.move} />
			</div>
		)
	}
}

export default Field;