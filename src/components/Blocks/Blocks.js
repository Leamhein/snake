import React, { Component } from 'react';
import './Blocks.css';

class Blocks extends Component {
	//every block looks like <div key='1_9' className='block snake'></div>
    render () {
        const blocks = [];
		for (let i = 0; i < 20; i++) {
			blocks.push([]);
			for (let j = 0; j < 20; j++) {
				blocks[i].push(
					<div key={i+'_'+j} className={'block '+(
                        this.props.snakePos.some((item)=>{
                            return item[0]===i && item[1]===j
                        }) ? 'snake ' : ''
                    ) + ([this.props.applePos].some((item)=>{
                        return item[0]===i && item[1]===j
                    }) ? 'apple ' : ''
                )}>
					</div>
				)
			}
		}

		return (
			<div className='blocks'>
				{blocks}
			</div>
		)
    }
}

export default Blocks